/* rho/test.h - v0.2.0 - MIT - Oleksandr Manenko, 2021
 * A header-only library for unit testing in C.
 *
 *******************************************************************************
 * USAGE
 *
 * #include <rho/test.h>
 *
 * int main( void ) {
 *     // A test prints a description and tests the expression.
 *     rho_test("One plus one is two", 1 + 1 == 2);
 *     // A test assert prints the expression it tests.
 *     rho_test_assert(12 + 5 == 17);
 * }
 *
 *******************************************************************************
 * CONFIGURATION
 *
 * You can define the following macros before including the header to change the
 * library's behavior:
 *
 * RHO_TEST_PRINTF(FMT, ...) - printf-like macro to print failed/succeeded
 * messages. Default is printf from stdio.
 *
 * RHO_TEST_EXIT(CODE) - called when a test failed to exit with the given status
 * code. Default is exit from stdlib.
 *
 * RHO_TEST_STR - a macro used to construct and validate a string literal.
 * Default is "" STR "" to make sure a users uses a true literal. You can
 * redefine it with u8"" u8##STR u8"" if you need UTF-8.
 *
 * RHO_TEST_MSG_FAILURE - string literal printed near the test message in case
 * of test failed.
 *
 * RHO_TEST_MSG_SUCCESS - string literal printed near the test message in case
 * of test succeeded.
 *
 * RHO_TEST_STRIP_TERM_FMT - define this to disable colour formatting of the
 * messages printed during the status code to a terminal.
 *
 * RHO_TEST_TERM_FMT_RESET - string literal with the escape sequence to reset
 * terminal's formatting.
 *
 * RHO_TEST_TERM_FMT_FAILURE - string literal with the escape sequence to set
 * terminal's formatting for printing RHO_TEST_MSG_FAILURE string literal.
 *
 * RHO_TEST_TERM_FMT_SUCCESS - string literal with the escape sequence to set
 * terminal's formatting for printing RHO_TEST_MSG_SUCCESS string literal.
 *
 *******************************************************************************
 * LICENSE
 *
 * Scroll to the end of the file for license information.
 */

#ifndef RHO_TEST_H_INCLUDED
#    define RHO_TEST_H_INCLUDED ( 1 )

#    ifdef __cplusplus
extern "C" {
#    endif

/* Interface ******************************************************************/

#    ifndef RHO_TEST_PRINTF
#        ifdef RHO_PRINTF
#            define RHO_TEST_PRINTF( FORMAT, ... )                             \
                RHO_PRINTF( FORMAT, __VA_ARGS__ )
#        else
#            include <stdio.h>
#            define RHO_TEST_PRINTF( FORMAT, ... ) printf( FORMAT, __VA_ARGS__ )
#        endif
#    endif

#    ifndef RHO_TEST_EXIT
#        ifdef RHO_EXIT
#            define RHO_TEST_EXIT( CODE ) RHO_EXIT( CODE )
#        else
#            include <stdlib.h>
#            define RHO_TEST_EXIT( CODE ) exit( CODE )
#        endif
#    endif

#    ifndef RHO_TEST_STR
#        ifdef RHO_STR
#            define RHO_TEST_STR( STR ) RHO_STR( STR )
#        else
#            define RHO_TEST_STR( STR ) "" STR ""
#        endif
#    endif

#    ifndef RHO_TEST_MSG_FAILURE
#        define RHO_TEST_MSG_FAILURE RHO_TEST_STR( "[-]" )
#    endif

#    ifndef RHO_TEST_MSG_SUCCESS
#        define RHO_TEST_MSG_SUCCESS RHO_TEST_STR( "[+]" )
#    endif

#    ifndef RHO_TEST_STRIP_TERM_FMT
#        ifndef RHO_TEST_TERM_FMT_RESET
#            define RHO_TEST_TERM_FMT_RESET RHO_TEST_STR( "\x1B[0m" )
#        endif
#        ifndef RHO_TEST_TERM_FMT_FAILURE
#            define RHO_TEST_TERM_FMT_FAILURE RHO_TEST_STR( "\x1B[31m" )
#        endif
#        ifndef RHO_TEST_TERM_FMT_SUCCESS
#            define RHO_TEST_TERM_FMT_SUCCESS RHO_TEST_STR( "\x1B[32m" )
#        endif
#    else
#        undef RHO_TEST_TERM_FMT_RESET
#        undef RHO_TEST_TERM_FMT_FAILURE
#        undef RHO_TEST_TERM_FMT_SUCCESS
#        define RHO_TEST_TERM_FMT_RESET   RHO_TEST_STR( "" )
#        define RHO_TEST_TERM_FMT_FAILURE RHO_TEST_STR( "" )
#        define RHO_TEST_TERM_FMT_SUCCESS RHO_TEST_STR( "" )
#    endif

#    define RHO_TEST_TERM_FMT_WRAP( CODE, MSG )                                \
        RHO_TEST_TERM_FMT_RESET CODE MSG RHO_TEST_TERM_FMT_RESET

#    define RHO_TEST_TERM_FMT_WRAP_FAILURE( MSG )                              \
        RHO_TEST_TERM_FMT_WRAP( RHO_TEST_TERM_FMT_FAILURE, MSG )
#    define RHO_TEST_TERM_FMT_WRAP_SUCCESS( MSG )                              \
        RHO_TEST_TERM_FMT_WRAP( RHO_TEST_TERM_FMT_SUCCESS, MSG )


#    define RHO_TEST_F RHO_TEST_TERM_FMT_WRAP_FAILURE( RHO_TEST_MSG_FAILURE )
#    define RHO_TEST_S RHO_TEST_TERM_FMT_WRAP_SUCCESS( RHO_TEST_MSG_SUCCESS )

#    define rho_test( DESC, COND )                                             \
        do {                                                                   \
            char const *desc = DESC;                                           \
            desc             = desc ? desc : #COND;                            \
            if ( !( COND ) ) {                                                 \
                RHO_TEST_PRINTF(                                               \
                    RHO_TEST_STR( "%s %s\n" ),                                 \
                    RHO_TEST_F,                                                \
                    desc );                                                    \
                RHO_TEST_PRINTF(                                               \
                    RHO_TEST_STR( "  at %s:%s:%d\n" ),                         \
                    __FILE__,                                                  \
                    __FUNCTION__,                                              \
                    __LINE__ );                                                \
                RHO_TEST_EXIT( 1 );                                            \
            } else {                                                           \
                RHO_TEST_PRINTF(                                               \
                    RHO_TEST_STR( "%s %s\n" ),                                 \
                    RHO_TEST_S,                                                \
                    desc );                                                    \
            }                                                                  \
        } while ( 0 )

#    define rho_test_assert( COND ) rho_test( NULL, COND )

#    ifdef __cplusplus
}
#    endif

#endif /* RHO_TEST_H_INCLUDED */

/* Copyright 2021 Oleksandr Manenko
 *
 * Permission is hereby granted, free of  charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction,  including without limitation the rights
 * to use,  copy, modify,  merge, publish,  distribute, sublicense,  and/or sell
 * copies  of the  Software,  and to  permit  persons to  whom  the Software  is
 * furnished to do so, subject to the following conditions:
 *
 * The above  copyright notice and this  permission notice shall be  included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE  IS PROVIDED "AS IS",  WITHOUT WARRANTY OF ANY  KIND, EXPRESS OR
 * IMPLIED,  INCLUDING BUT  NOT LIMITED  TO THE  WARRANTIES OF  MERCHANTABILITY,
 * FITNESS FOR A  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO  EVENT SHALL THE
 * AUTHORS  OR COPYRIGHT  HOLDERS  BE LIABLE  FOR ANY  CLAIM,  DAMAGES OR  OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
