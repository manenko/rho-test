#include <rho/test.h>

int main( void )
{
    rho_test( "One plus one is two", 1 + 1 == 2 );
    rho_test_assert( 40 + 2 > 10 );
    rho_test( "This test fails and terminates program execution", 7 + 8 < 1 );
    rho_test(
        "I will not be executed unless you redefine RHO_TEST_EXIT to something "
        "that does not terminate the program",
        7 - 1 == 0 );

    /* Program output:
     *
     * [+] One plus one is two
     * [+] 40 + 2 > 10
     * [-] This test fails and terminates program execution
     *   at /home/blah/blah/rho_test/test/main.c:main:11
     */

    return 0;
}
