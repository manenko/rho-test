# RhoTest

A simple header-only C unit-testing library.

## Getting started

### Add library to the project

To use the library you have to include it to your project. There are a few options, for example:

- Manually copy `include/rho` directory to the project header directory. This is the simplest one.
- Add the library's repository as a git submodule and configure your build system to look for headers inside `$submodule/include` directory. Make sure you know what are you doing.
- Use CMake and FetchContent. Fetch the library:

  ```cmake
  include(FetchContent)
  FetchContent_Declare(RhoTest
    GIT_REPOSITORY https://gitlab.com/manenko/rho-test.git
    GIT_TAG        v0.2.0)
  FetchContent_MakeAvailable(RhoTest)
  ```

  Then link your CMake target with the library:

  ```cmake
  target_link_libraries(${PROJECT_NAME} PRIVATE RhoTest)
  ```

### Optional configuration

You can define the following macros before including the header to change the
library's behavior:

`RHO_TEST_PRINTF(FMT, ...)` - printf-like macro to print failed/succeeded
messages to the terminal. Default is `printf` from `stdio`.

`RHO_TEST_EXIT(CODE)` - called when a test failed to exit with the given status
code. Default is `exit` from `stdlib`.

`RHO_TEST_STR` - a macro used to construct and validate a string literal.
Default is `"" STR ""` to make sure a users uses a true literal. You can
redefine it with `u8"" u8##STR u8""` if you need UTF-8.

`RHO_TEST_MSG_FAILURE` - string literal printed near the test message in case
of test failed. Default is `[-]`.

`RHO_TEST_MSG_SUCCESS` - string literal printed near the test message in case
of test succeeded. Default is `[+]`.

`RHO_TEST_STRIP_TERM_FMT` - define this to disable formatting of the
messages printed during the tests' execution to a terminal.

`RHO_TEST_TERM_FMT_RESET` - string literal with the escape sequence to reset
terminal's formatting.

`RHO_TEST_TERM_FMT_FAILURE` - string literal with the escape sequence to set
terminal's formatting for printing `RHO_TEST_MSG_FAILURE` string literal.

`RHO_TEST_TERM_FMT_SUCCESS` - string literal with the escape sequence to set
terminal's formatting for printing `RHO_TEST_MSG_SUCCESS` string literal.


## Example

```c
#include <rho/test.h>

int main( void )
{
    rho_test( "One plus one is two", 1 + 1 == 2 );
    rho_test_assert( 40 + 2 > 10 );
    rho_test( "This test fails and terminates program execution", 7 + 8 < 1 );
    rho_test(
        "I will not be executed unless you redefine RHO_TEST_EXIT to something "
        "that does not terminate the program",
        7 - 7 == 0 );

    /* Program output:
     *
     * [OK] One plus one is two
     * [OK] 40 + 2 > 10
     * [ERROR] This test fails and terminates program execution
     *   at /home/dha/werda/test/main.c:main:11
     */

    return 0;
}
```

## CI/CD configuration

Configure the library to strip colour formatting from the output and to
continue to run tests even after encountering a failed assertion:

```c
#define RHO_TEST_STRIP_TERM_FMT
#define RHO_TEST_EXIT(CODE) (void)(CODE)
#include <rho/test.h>
```

Then you use `grep` on your CI/CD server:

```shell
./test/your-tests-executable | grep -A1 "^\[-\]" | grep -v -- "^--$"
```

The first `grep` filters out all successful tests, and the second one removes
group separators added by the first `grep`.

If the output is empty, then there are no failed tests, otherwise grep prints
this:

```text
[-] This test fails and terminates program execution
  at /home/dha/werda/test/main.c:main:9
[-] I will not be executed unless you redefine RHO_TEST_EXIT to something that does not terminate the program
  at /home/dha/werda/test/main.c:main:13
```
  
Then you can either use this as a test report or transform it further.

## License

MIT
