cmake_minimum_required(VERSION 3.17 FATAL_ERROR)

project(RhoTest
  VERSION      0.2.0
  DESCRIPTION  "C header-only unit testing library."
  HOMEPAGE_URL "https://gitlab.com/manenko/rho-test"
  LANGUAGES    C)

# This name is used to make it possible to have different versions of the
# library on the same system.
set(_VERSIONED_PROJECT_NAME ${PROJECT_NAME}-${PROJECT_VERSION})

add_library(${PROJECT_NAME} INTERFACE)
add_subdirectory(test)

include(GNUInstallDirs)

target_include_directories(${PROJECT_NAME}
  INTERFACE
  $<BUILD_INTERFACE:${${PROJECT_NAME}_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${_VERSIONED_PROJECT_NAME}>)

target_sources(${PROJECT_NAME} PRIVATE include/rho/test.h)
target_compile_features(${PROJECT_NAME} INTERFACE c_std_11)

set_property(TARGET ${PROJECT_NAME} PROPERTY C_STANDARD          11)
set_property(TARGET ${PROJECT_NAME} PROPERTY C_STANDARD_REQUIRED ON)

install(
  TARGETS ${PROJECT_NAME}
  EXPORT  ${PROJECT_NAME}Targets
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

include(CMakePackageConfigHelpers)
write_basic_package_version_file("${PROJECT_NAME}ConfigVersion.cmake"
  VERSION       ${PROJECT_VERSION}
  COMPATIBILITY SameMajorVersion)

configure_package_config_file(
  "${PROJECT_SOURCE_DIR}/cmake/ProjectConfig.cmake.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  INSTALL_DESTINATION
  ${CMAKE_INSTALL_DATAROOTDIR}/${_VERSIONED_PROJECT_NAME}/cmake)

install(
  EXPORT      ${PROJECT_NAME}Targets
  FILE        ${PROJECT_NAME}Targets.cmake
  NAMESPACE   ${PROJECT_NAME}::
  DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/${_VERSIONED_PROJECT_NAME}/cmake)

install(
  FILES
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"

  DESTINATION
  ${CMAKE_INSTALL_DATAROOTDIR}/${_VERSIONED_PROJECT_NAME}/cmake)

install(
  DIRECTORY   ${PROJECT_SOURCE_DIR}/include/
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/${_VERSIONED_PROJECT_NAME})

